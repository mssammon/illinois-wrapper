package edu.illinois.cs.cogcomp.sim;

import java.io.IOException;

import edu.illinois.cs.cogcomp.common.WnsimConstants;
import edu.illinois.cs.cogcomp.config.WNSimConfigurator;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.wordnet.ImprovedWN;

/**
 * Class to calculate Wordnet path based similarity
 * 
 * @author John Wieting
 * @author mssammon
 * @author ngupta18
 * @author sgupta96
 *
 */

/* TODO: Check if the @MetricWord class can be used instead of String */ 
public class WordNetPathSim extends StringMetric<String> {

	private static final String NAME = WordNetPathSim.class.getCanonicalName();
	private ImprovedWN iwn;
	
	/**
	 * Contains sample invocation
	 * @param args optional: config file containing path to the wordnet resource, as in config/sampleConfig.txt
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		WordNetPathSim wnp = null;
		if (args.length == 1)
			wnp = new WordNetPathSim(new ResourceManager(args[0]));
		else
			wnp = new WordNetPathSim();

		String dog = "dog";
		String animal = "animal";
		
		System.out.println("Similarity b/w dog and animal is " + wnp.compare(dog, animal));
		System.out.println("Similarity b/w animal and dog is " + wnp.compare(animal, dog));
	}
	
	/**
	 * Constructor
	 * 
	 * @param rm ResourceManager with non-default properties set, including path to the local Wordnet resource
	 * @throws IOException
	 */
	public WordNetPathSim(ResourceManager rm) throws IOException {
		initialize(new WNSimConfigurator().getConfig(rm), false);
	}

	/**
	 * default constructor: expects all resources to be defaults, and will look for
	 * them on classpath
	 *   
	 * @throws IOException
	 */
	public WordNetPathSim() throws IOException {
		initialize(new WNSimConfigurator().getDefaultConfig(), true);
	}

	private void initialize(ResourceManager rm, boolean useJar) throws IOException {
		String wnPath = rm.getString(WnsimConstants.WNPATH);
		iwn = new ImprovedWN(wnPath, useJar);
	}

	/**
	 * Calculates similarity between two words
	 * 
	 * May be assymmetric.
	 * 
	 * @param small 1st word
	 * @param big 2nd word
	 * @return similarity score between word1 and word2
	 */
	private double wordScore(String small, String big) {
		return iwn.findPath(small, big);
	}


	/**
	 * Calculates similarity between two words
	 *
	 * May be assymmetric.
	 *
	 * @param arg1 1st word
	 * @param arg2 2nd word
	 * @return similarity score between arg1 and arg2 plus a "reason"
	 */
	public MetricResponse compare(String arg1, String arg2) {
		return new MetricResponse(wordScore(arg1, arg2), NAME);
	}

	/**
	 * returns the name of this metric. Used as the reason by the default {@link #compareStringValues } method.
	 *
	 * @return the name of this metric.
	 */
	@Override
	public String getName() {
		return NAME;
	}

	/**
	 * construct a T instance from just a String to allow the {@link #compareStringValues} to interact
	 * with {@link Metric <T>.compare }
	 *
	 * @param word the word to wrap
	 * @return the corresponding object of type T specified by the implementor
	 */
	@Override
	protected String wrapStringArgument(String word) {
		return word;
	}
}
