package edu.illinois.cs.cogcomp.sim;

import java.io.IOException;
import java.util.Map;

import edu.cmu.lti.ws4j.WS4J;
import edu.illinois.cs.cogcomp.common.WnsimConstants;
import edu.illinois.cs.cogcomp.config.WNSimConfigurator;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.wordnet.ImprovedWN;

/**
 * Class to calculate Wordnet based similarity between two words using metric by Lin, Resnik
 * 
 * @author John Wieting
 * @author mssammon
 * @author ngupta18
 * @author sgupta96
 *
 */
public class WordSim extends StringMetric<MetricWord> {

	private static final String NAME = WordSim.class.getCanonicalName();
	public String method;
	private ImprovedWN iwn;
	
	/**
	 * Contains sample invocation
	 * @param args optional: config file containing path to the wordnet resource, as in config/sampleConfig.txt
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		WordSim wsimL = null;
		WordSim wsimR = null;
		WordSim wsim = null;
		if (args.length == 1) {
			wsimL = new WordSim(WnsimConstants.LIN, new ResourceManager(args[0]));
			wsimR = new WordSim(WnsimConstants.RESNIK, new ResourceManager(args[0]));
			wsim = new WordSim(new ResourceManager(args[0]));
		}
		else {
			wsimL = new WordSim(WnsimConstants.LIN);
			wsimR = new WordSim(WnsimConstants.RESNIK);
			wsim = new WordSim();
		}

		String dog = "dog";
		String animal = "animal";
		
		System.out.println("Similarity b/w dog and animal by Lin's method is " + wsimL.compareString(dog, animal));
		System.out.println("Similarity b/w animal and dog by Lin's method is " + wsimL.compareString(animal, dog));
		
		System.out.println("Similarity b/w dog and animal by Resnik's method is " + wsimR.compareString(dog, animal));
		System.out.println("Similarity b/w animal and dog by Resnik's method is " + wsimR.compareString(animal, dog));
		
		System.out.println("Default Similarity (Lin) b/w dog and animal is " + wsim.compareString(dog, animal));
		System.out.println("Default Similarity (Lin) b/w animal and dog is " + wsim.compareString(animal, dog));
		
		System.out.println("Similarity b/w dog and animal by Resnik's method is " + wsim.compareString(dog, animal, "Resnik"));
		System.out.println("Similarity b/w animal and dog by Resnik's method is " + wsim.compareString(animal, dog, "Resnik"));
	}
	
	/**
	 * default constructor: expects all resources to be defaults, and will look for
	 * them on classpath
	 *
	 * @param type which of the similarity method from {Lin, Resnik} to use   
	 * @throws IOException
	 */
	//TODO: Use Enum for valid similarity types
	public WordSim(String type) throws IOException {
		method = type;
		initialize(new WNSimConfigurator().getDefaultConfig(), true);
	}
	
	/**
	 * default constructor: expects all resources to be defaults, and will look for
	 * them on classpath
	 *
	 * Uses Lin's similarity metric by default   
	 * @throws IOException
	 */
	public WordSim() throws IOException {
		this(WnsimConstants.LIN);
	}
	
	/**
	 * Constructor
	 * 
	 * @param type which of the similarity method from {Lin, Resnik} to use
	 * @param rm ResourceManager containing non-default config options, including path to the local Wordnet resource
	 * @throws IOException
	 */ 
	 //TODO: Use Enum for valid similarity types
	public WordSim(String type, ResourceManager rm) throws IOException {
		ResourceManager finalRm = new WNSimConfigurator().getConfig(rm);
		method = type;
		initialize(finalRm, false);
	}
	
	/**
	 * Constructor
	 * 
	 * Uses Lin's similarity metric by default   
	 * @param rm ResourceManager containing non-default config options, including path to the local Wordnet resource
	 * 
	 * @throws IOException
	 */ 
	public WordSim(ResourceManager rm) throws IOException {
		this(WnsimConstants.LIN, rm);
	}
	
	private void initialize(ResourceManager rm, boolean useJar) throws IOException {
		String wnPath = rm.getString(WnsimConstants.WNPATH);
		iwn = new ImprovedWN(wnPath, useJar);
	}
	
	/**
	 * Calculates similarity between two words according to the configured metric type
	 * 
	 * May be assymmetric.
	 *
	 * @param small 1st word
	 * @param big 2nd word
	 * @return similarity score between word1 and word2 plus a "reason"
	 */
	public MetricResponse compareString(String small, String big) {
		return compareString(small, big, method);
	}
	
	/**
	 * Calculates similarity between two words according to the configured metric type
	 * 
	 * May be assymmetric.
	 *
	 * @param small 1st word
	 * @param big 2nd word
	 * @param method Similarity method to be used out of {Lin, Resnik}
	 * @return similarity score between word1 and word2 plus a "reason"
	 */ 
	public MetricResponse compareString(String small, String big, String method) {
		double score = 0;

		if( !(iwn.getAllSynset(small).size() == 0 || iwn.getAllSynset(big).size() == 0) )
		if(method.equals(WnsimConstants.LIN)) {
			score = WS4J.calcSimilarityByLin(small, big);
		}
		else if(method.equals(WnsimConstants.RESNIK)) {
			score = WS4J.calcSimilarityByResnik(small, big);
		}
		else
			throw new IllegalArgumentException("Requires an argument out of {Lin, Resnik}");

		return new MetricResponse(score, NAME + ":" + method);
	}
	
	/**
	 * Calculates similarity between two words according to the configured metric type, using POS tags if provided.
	 * Currently ignores POS.
	 * 
	 * May be assymmetric.
	 *
	 * @param arg1 1st word
	 * @param arg2 2nd word
	 * 
	 * @return similarity score between word1 and word2 plus a "reason"
	 * @throws IllegalArgumentException
	 */ 
	public MetricResponse compare(MetricWord arg1, MetricWord arg2) throws IllegalArgumentException {
		return compare(arg1, arg2, method);
	}
	
	/**
	 * Calculates similarity between two words according to the configured metric type, using POS tags if provided.
	 * Currently ignores POS.
	 * 
	 * May be assymmetric.
	 *
	 * @param arg1 1st word
	 * @param arg2 2nd word
	 * @param method Similarity method to be used out of {Lin, Resnik}
	 * 
	 * @return similarity score between word1 and word2 plus a "reason"
	 * @throws IllegalArgumentException
	 */ 
	public MetricResponse compare(MetricWord arg1, MetricWord arg2, String method) throws IllegalArgumentException {
		return compareString(arg1.word, arg2.word, method);
	}

	/**
	 * returns the name of this metric. Used as the reason by the default {@link #compareStringValues } method.
	 *
	 * @return the name of this metric.
	 */
	@Override
	public String getName() {
		return NAME;
	}

	/**
	 * construct a T instance from just a String to allow the {@link #compareStringValues} to interact
	 * with {@link Metric <T>.compare }
	 *
	 * @param word the word to wrap
	 * @return the corresponding object of type T specified by the implementor
	 */
	@Override
	protected MetricWord wrapStringArgument(String word) {
		return new MetricWord( word, null );
	}
	
    /**
     * alternate representation of the compare() method using only Map and String values, to support
     * XMLRPC-like interfaces
     *
     * @param arguments a map of key-value pairs, where each key indicates the intended use of the corresponding value.
     *                  Must allow the implementor to map these values into the data structures used by
     *                  {@link Metric <T>.compare}.
     * @return a map of key-value pairs, where they keys indicate the intended use of the entry
     */
	@Override
    public Map<String, String> compareStringValues(Map<String, String> arguments) {
		String metric = arguments.get( WnsimConstants.METRIC );
		if ( metric != null ) {
            this.method = metric;
        }
		
		return super.compareStringValues(arguments);
    }
}
