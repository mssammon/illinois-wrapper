package edu.illinois.cs.cogcomp.sim;

import java.io.IOException;

import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;

/**
 * Simpler WNSim API that takes only String arguments
 *
 * Created by mssammon on 1/8/16.
 */

public class WNSimSimple extends StringMetric< String > {
	private static final String NAME = WNSimSimple.class.getCanonicalName();
    private WNSim metric;

	/**
	 * Contains sample invocation
	 * @param args optional: config file containing path to the wordnet resource, as in config/sampleConfig.txt
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		WNSimSimple wnsimS = null;
		if (args.length == 1)
			wnsimS = new WNSimSimple(new ResourceManager(args[0]));
		else
			wnsimS = new WNSimSimple();

		String dog = "dog";
		String animal = "animal";
		
		System.out.println("Similarity b/w dog and animal is " + wnsimS.compare(dog, animal));
		System.out.println("Similarity b/w animal and dog is " + wnsimS.compare(animal, dog));
	}
    
	/**
	 * default constructor: expects all resources to be defaults, and will look for
	 *    them on classpath
	 *    
	 * @throws IOException   
	 */
    public WNSimSimple() throws IOException {
        metric = new WNSim();
    }
    
    /**
	 * Constructor
	 * 
	 * @param rm ResourceManager containing non-default config options, including path to the local Wordnet resource
	 * @throws IOException
	 */
	public WNSimSimple(ResourceManager rm) throws IOException {
		metric = new WNSim(rm);
	}

    /**
	 * Calculates similarity between two words
	 *
	 * @param arg1 1st word
	 * @param arg2 2nd word
	 * @return similarity score between arg1 and arg2 plus a "reason"
	 * throws IllegalArgumentException
	 */
    public MetricResponse compare(String arg1, String arg2) throws IllegalArgumentException {
        return new MetricResponse(metric.compare(arg1, arg2).score, NAME);
    }

	/**
	 * returns the name of this metric. Used as the reason by the default {@link #compareStringValues } method.
	 *
	 * @return the name of this metric.
	 */
	@Override
	public String getName() {
		return NAME;
	}

	/**
	 * construct a T instance from just a String to allow the {@link #compareStringValues} to interact
	 * with {@link Metric <T>.compare }
	 *
	 * @param word the word to wrap
	 * @return the corresponding object of type T specified by the implementor
	 */
	@Override
	protected String wrapStringArgument(String word) {
		return word;
	}

}
