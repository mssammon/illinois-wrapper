package edu.illinois.cs.cogcomp.sim;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import edu.mit.jwi.item.POS;

/**
 * Data structure for WNSim metric, containing a String and a value for
 *  Part of Speech (may be UNKNOWN).
 *
 * @author mssammon
 * @author sgupta96
 * @author ngupta18
 */
public class MetricWord implements Serializable{
	private static final long serialVersionUID = 1L;
	public String word;
    public POS pos;

    public MetricWord( String word, POS pos ) {
        this.word = word;
        this.pos = pos;
    }
        
    public void writeObject(ObjectOutputStream out) throws IOException {
    	out.writeObject(word);
    	out.writeObject(pos);
    }

    public void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        word = (String) in.readObject();
        pos = (POS) in.readObject();
    }
      
}
