package edu.illinois.cs.cogcomp.wrapper;

import edu.illinois.cs.cogcomp.sim.WNSim;

import java.io.*;
import java.util.HashMap;

public class Wrapper {

    public static HashMap<SimilarityTechnique, String> embeddingFilePaths;
    public static WNSim wnsim;
    static {
        embeddingFilePaths = new HashMap<>();
        embeddingFilePaths.put(SimilarityTechnique.paragram_300_ws353, "C:\\Users\\Kevin\\Desktop\\SU16\\CogComp\\paragram_data\\paragram_300_ws353.txt");
        embeddingFilePaths.put(SimilarityTechnique.paragram_300_sl999, "C:\\Users\\Kevin\\Desktop\\SU16\\CogComp\\paragram_data\\paragram_300_sl999.txt");

        embeddingFilePaths.put(SimilarityTechnique._25_cbow_4, "C:\\Users\\Kevin\\Desktop\\SU16\\CogComp\\eval_word_data\\25_cbow_4.txt");
        embeddingFilePaths.put(SimilarityTechnique._25_cbow_6, "C:\\Users\\Kevin\\Desktop\\SU16\\CogComp\\eval_word_data\\25_cbow_6.txt");
        embeddingFilePaths.put(SimilarityTechnique._25_cbow_8, "C:\\Users\\Kevin\\Desktop\\SU16\\CogComp\\eval_word_data\\25_cbow_8.txt");
        embeddingFilePaths.put(SimilarityTechnique._25_cbow_binary, "C:\\Users\\Kevin\\Desktop\\SU16\\CogComp\\eval_word_data\\25_cbow_binary.txt");

        embeddingFilePaths.put(SimilarityTechnique._25_skip_4, "C:\\Users\\Kevin\\Desktop\\SU16\\CogComp\\eval_word_data\\25_skip_4.txt");
        embeddingFilePaths.put(SimilarityTechnique._25_skip_6, "C:\\Users\\Kevin\\Desktop\\SU16\\CogComp\\eval_word_data\\25_skip_6.txt");
        embeddingFilePaths.put(SimilarityTechnique._25_skip_8, "C:\\Users\\Kevin\\Desktop\\SU16\\CogComp\\eval_word_data\\25_skip_8.txt");
        embeddingFilePaths.put(SimilarityTechnique._25_skip_binary, "C:\\Users\\Kevin\\Desktop\\SU16\\CogComp\\eval_word_data\\25_skip_binary.txt");

        embeddingFilePaths.put(SimilarityTechnique._200_cbow_4, "C:\\Users\\Kevin\\Desktop\\SU16\\CogComp\\eval_word_data\\200_cbow_4.txt");
        embeddingFilePaths.put(SimilarityTechnique._200_cbow_6, "C:\\Users\\Kevin\\Desktop\\SU16\\CogComp\\eval_word_data\\200_cbow_6.txt");
        embeddingFilePaths.put(SimilarityTechnique._200_cbow_8, "C:\\Users\\Kevin\\Desktop\\SU16\\CogComp\\eval_word_data\\200_cbow_8.txt");
        embeddingFilePaths.put(SimilarityTechnique._200_cbow_binary, "C:\\Users\\Kevin\\Desktop\\SU16\\CogComp\\eval_word_data\\200_cbow_binary.txt");

        embeddingFilePaths.put(SimilarityTechnique._200_skip_4, "C:\\Users\\Kevin\\Desktop\\SU16\\CogComp\\eval_word_data\\200_skip_4.txt");
        embeddingFilePaths.put(SimilarityTechnique._200_skip_6, "C:\\Users\\Kevin\\Desktop\\SU16\\CogComp\\eval_word_data\\200_skip_6.txt");
        embeddingFilePaths.put(SimilarityTechnique._200_skip_8, "C:\\Users\\Kevin\\Desktop\\SU16\\CogComp\\eval_word_data\\200_skip_8.txt");
        embeddingFilePaths.put(SimilarityTechnique._200_skip_binary, "C:\\Users\\Kevin\\Desktop\\SU16\\CogComp\\eval_word_data\\200_skip_binary.txt");

        try {
            wnsim = new WNSim();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public enum SimilarityTechnique {
        WNSim,
        paragram_300_ws353, paragram_300_sl999,
        _25_cbow_4, _25_cbow_6, _25_cbow_8, _25_cbow_binary,
        _25_skip_4, _25_skip_6, _25_skip_8, _25_skip_binary,
        _200_cbow_4, _200_cbow_6, _200_cbow_8, _200_cbow_binary,
        _200_skip_4, _200_skip_6, _200_skip_8, _200_skip_binary,
    }

    public static void main (String[] args) {

        String stringA = "man";
        String stringB = "woman";

        for (int st=0; st<SimilarityTechnique.values().length; st++) {
            System.out.println(similarity(stringA, stringB, SimilarityTechnique.values()[st]));
        }

        System.out.println("Done");
    }

    public static double similarity(String stringA, String stringB, SimilarityTechnique similarityTechnique) {
        if (similarityTechnique != SimilarityTechnique.WNSim) {
            return cosine_similarity(stringA, stringB, embeddingFilePaths.get(similarityTechnique));
        } else {
            return wnsim.similarity(stringA, stringB);
        }
    }

    public static double cosine_similarity(String stringA, String stringB, String embeddingFilePath) {

        stringA = stringA.toLowerCase();
        stringB = stringB.toLowerCase();

        String[] stringSplitA = null;
        String[] stringSplitB = null;

        String thisLine = null;
        try {
            BufferedReader br = new BufferedReader(new FileReader(embeddingFilePath));
            while ((thisLine = br.readLine()) != null && (stringSplitA == null || stringSplitB == null)) {
                String[] thisEmbedding = thisLine.split(" ");
                if (thisEmbedding[0].equals(stringA)) {
                    //System.out.println(thisLine);
                    stringSplitA = thisEmbedding;
                }
                //not using "else" because the two words could be the same
                if (thisEmbedding[0].equals(stringB)) {
                    //System.out.println(thisLine);
                    stringSplitB = thisEmbedding;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        boolean not_found = false;
        if (stringSplitA == null) {
            System.out.println(stringA + " is not in the embedding file. ");
            not_found = true;
        }
        if (stringSplitB == null) {
            System.out.println(stringB + " is not in the embedding file. ");
            not_found = true;
        }
        if (not_found) {
            return 0.0;//equivalent to saying that there is no evidence for similarity (positive number) or disimilarity (negative number)
        }

        double[] embeddingA = new double[stringSplitA.length - 1];
        double[] embeddingB = new double[stringSplitB.length - 1];
        for (int i=0; i<embeddingA.length; i++) {
            embeddingA[i] = Double.parseDouble(stringSplitA[i+1]);
            embeddingB[i] = Double.parseDouble(stringSplitB[i+1]);
        }

        return cosine_similarity(embeddingA, embeddingB);
    }

    public static double cosine_similarity(double[] vectorA, double[] vectorB) {
        return dot_product(vectorA, vectorB) / magnitude(vectorA) / magnitude(vectorB);
    }

    public static double dot_product(double[] vectorA, double[] vectorB) {
        double dot_product = 0.0;
        for (int i=0; i<vectorA.length; i++) {
            dot_product += vectorA[i] * vectorB[i];
        }
        return dot_product;
    }

    public static double magnitude(double[] vector) {
        double magnitude = 0.0;
        for (int i=0; i<vector.length; i++) {
            magnitude += vector[i] * vector[i];
        }
        return Math.sqrt(magnitude);
    }

    public static void printFilePath(String embeddingFilePath) {
        String thisLine = null;
        try {
            // open input stream test.txt for reading purpose.
            BufferedReader br = new BufferedReader(new FileReader(embeddingFilePath));
            while ((thisLine = br.readLine()) != null) {
                System.out.println(thisLine);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
