package edu.illinois.cs.cogcomp.wnsim.xmlrpc;

import org.apache.xmlrpc.server.PropertyHandlerMapping;
import org.apache.xmlrpc.server.XmlRpcServer;
import org.apache.xmlrpc.server.XmlRpcServerConfigImpl;
import org.apache.xmlrpc.webserver.WebServer;

import edu.illinois.cs.cogcomp.sim.Paraphrase;
import edu.illinois.cs.cogcomp.sim.WNSim;
import edu.illinois.cs.cogcomp.sim.WordNetPathSim;
import edu.illinois.cs.cogcomp.sim.WordSim;

/**
 * XMLRpc server for Word Comparisons
 * 
 * @author sgupta96
 * @author ngupta18
 */
public class WCServer {

	/**
	 * @param args: port
	 */
	
	public static void main(String[] args) {

		if (args.length != 1) {
			System.err.println("Usage: WCServer port");
		}
		
		int port = (new Integer(args[0])).intValue();
		
		try {
			
			System.out.println("Attempting to start XML-RPC Server...");

			WebServer server = new WebServer( port );

			XmlRpcServer xmlRpcServer = server.getXmlRpcServer();

			PropertyHandlerMapping phm = new PropertyHandlerMapping();

			phm.addHandler("WNSim", WNSim.class);
			phm.addHandler("WordNetPathSim", WordNetPathSim.class);
			phm.addHandler("WordSim", WordSim.class);
			phm.addHandler("Paraphrase", Paraphrase.class);

			xmlRpcServer.setHandlerMapping(phm);
			XmlRpcServerConfigImpl serverConfig = (XmlRpcServerConfigImpl) xmlRpcServer.getConfig();
			serverConfig.setEnabledForExtensions(true);
			serverConfig.setContentLengthOptional(false);

			server.start();

			System.out.println("Started successfully.");
			System.out.println("Accepting requests. (Halt program to stop.)");
			
		} catch (Exception exception) {
			System.err.println("WCServer: " + exception);
		}
	}
	
}
