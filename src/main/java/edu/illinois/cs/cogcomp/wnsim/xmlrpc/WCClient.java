package edu.illinois.cs.cogcomp.wnsim.xmlrpc;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import edu.illinois.cs.cogcomp.common.WnsimConstants;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcCommonsTransportFactory;

import edu.illinois.cs.cogcomp.sim.MetricResponse;

/**
 * XMLRpc client for Word Comparisons
 *
 * @author sgupta96
 * @author ngupta18
 */
public class WCClient {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		if (args.length != 2) {
			System.err.println( "Usage: WCClient host port");
            System.exit( -1 );
		}

		String host = new String(args[0]);
		int port = (new Integer(args[1])).intValue();

		try {

			XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
			config.setServerURL( new URL("http://" + host + ":" + port + "/xmlrpc") );
			config.setEnabledForExtensions(true);
			config.setConnectionTimeout(60 * 1000);
			config.setReplyTimeout(60 * 1000);

			XmlRpcClient client = new XmlRpcClient();
			client.setTransportFactory(new XmlRpcCommonsTransportFactory(client));
			client.setConfig(config);

			String first = "dog";
			String second = "animal";
			String wordSimLin = "Lin";
			String wordSimResnik = "Resnik";

//			Object[] params = new Object[]{ first, second };
//
//            Object[] wordSimLParams =  new Object[]{ first, second, wordSimLin };;
//
//            Object[] wordSimRParams = new Object[]{ first, second, wordSimResnik };

            Map< String, String > inputs = new HashMap<String, String>();
            addWordArgsToParams( inputs, first, second );
            Object[] params = new Object[]{ inputs };

			Map<String, String> wnSimInitResult = (Map< String, String >)client.execute("WNSim.compareStringValues", params );
			MetricResponse wnSimResult = getMetricResponse( wnSimInitResult );


			System.out.println("Score: " + wnSimResult.score);
			System.out.println("Reason: " + wnSimResult.reason);


            Map<String, String> wnPathSimInitResult = (Map< String, String >) client.execute("WordNetPathSim.compareStringValues", params);
            MetricResponse wnPathSimResult = getMetricResponse( wnPathSimInitResult );
			System.out.println("Score: " + wnPathSimResult.score);
			System.out.println("Reason: " + wnPathSimResult.reason);

			Map<String, String> paraphraseSimInitResult = (Map< String, String >) client.execute("Paraphrase.compareStringValues", params);
            MetricResponse paraphraseSimResult = getMetricResponse( paraphraseSimInitResult );
			System.out.println("Score: " + paraphraseSimResult.score);
			System.out.println("Reason: " + paraphraseSimResult.reason);

            Map< String, String > wordSimLInputs = new HashMap<String, String>();
            addWordArgsToParams( wordSimLInputs, first, second );
            addMetricToParams( wordSimLInputs, wordSimLin );
            Object[] wordSimLParams = new Object[]{ wordSimLInputs };

			Map<String, String> wordSimLInitResult = (Map< String, String >)client.execute("WordSim.compareStringValues", wordSimLParams);
            MetricResponse wordSimLResult = getMetricResponse( wordSimLInitResult );
			System.out.println("Score: " + wordSimLResult.score);
			System.out.println("Reason: " + wordSimLResult.reason);

            Map< String, String > wordSimRInputs = new HashMap<String, String>();
            addWordArgsToParams(wordSimRInputs, first, second );
            addMetricToParams( wordSimRInputs, wordSimResnik );
            Object[] wordSimRParams = new Object[]{ wordSimRInputs };
            
            Map<String, String> wordSimRInitResult = (Map< String, String >) client.execute("WordSim.compareStringValues", wordSimRParams);
            MetricResponse wordSimRResult = getMetricResponse( wordSimRInitResult );
            System.out.println("Score: " + wordSimRResult.score);
			System.out.println("Reason: " + wordSimRResult.reason);

		} catch (Exception exception) {
			System.err.println("JavaClient: " + exception);
		}
	}

    private static void addMetricToParams(Map< String, String> params, String metricName) {
        params.put( WnsimConstants.METRIC, metricName );
    }

    private static void addWordArgsToParams(Map<String, String> params, String first, String second) {
        params.put( WnsimConstants.FIRST_WORD, first );
        params.put( WnsimConstants.SECOND_WORD, second );
    }

    private static MetricResponse getMetricResponse(Map<String, String> wnSimInitResult) {
		String reason = wnSimInitResult.get(WnsimConstants.REASON );
		double score = Double.parseDouble( wnSimInitResult.get( WnsimConstants.SCORE ) );
		return new MetricResponse(score, reason);
	}

}
