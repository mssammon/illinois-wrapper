package edu.illinois.cs.cogcomp.common;

/**
 * constant values used in wnsim
 *
 * Created by mssammon on 12/17/14.
 */
public class WnsimConstants {
    public static String LIN = "Lin";
    public static String BAL = "BAL";
    public static String COS = "COS";

    public static String RESNIK = "Resnik";
    public static final String WNPATH = "wnPath"; // path for wordnet dump
    public static final String PARAPATH = "paraphrasePath"; // path for file containing similarity scores extracted from PPDB

    // support web service API by providing parameter names
    public static final String FIRST_WORD = "FIRST_STRING";
    public static final String SECOND_WORD = "SECOND_STRING";
    public static final String REASON = "REASON";
    public static final String SCORE = "SCORE";
    public static final String METRIC = "METRIC";

}
