package edu.illinois.cs.cogcomp.sim;

import org.junit.Test;

import edu.illinois.cs.cogcomp.common.WnsimConstants;

import java.io.IOException;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * simple test of WNSim functionality
 * Created by mssammon on 12/18/14.
 */
public class WnsimTest {

    private static final String NAME = WnsimTest.class.getCanonicalName();

    /**
     * basic tests of WNSim functionality.
     * 
     */
    @Test
    public void testWnsim() {

        WNSim sim = null;
        try {
            sim = new WNSim();
        } catch (IOException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        double successScore = 0.01;

        String first = "apple";
        String second = "fruit";
        String third = "church";
        String fourth = "building";
        String fifth = "dog";
        String sixth = "animal";

        MetricResponse response = sim.compare(first, second);
        assertTrue(response.score > successScore);
        printResult(first, second, response);

        response = sim.compare(first, third);
        assertTrue(response.score < successScore);
        printResult(first, third, response);

        response = sim.compare(third, fourth);
        assertTrue(response.score > successScore);
        printResult(third, fourth, response);

        response = sim.compare(fifth, sixth );
        assertTrue(response.score > successScore);
        printResult(fifth, sixth, response);
    }

    @Test
    public void testParaphrase() {
        Paraphrase p = null;
        try {
            p = new Paraphrase();
        } catch (IOException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
        double successScore = 0.01;
        String wife = "wife";
		String woman = "woman";
		String dog = "dog";
		String animal = "animal";
		String show = "show";
		String found = "found";
		String wound = "wound";
		String injure = "injure";

        MetricResponse response = p.compareString(wife, woman);
        assertTrue(response.score > successScore);
        printResult(wife, woman, response);

        response = p.compareString(woman, wife);
        assertTrue(response.score > successScore);
        printResult(woman, wife, response);

        response = p.compareString(dog, animal);
        assertTrue(response.score > successScore);
        printResult(dog, animal, response);

        response = p.compareString(show, found);
        assertTrue(response.score > successScore);
        printResult(show, found, response);

        response = p.compareString(wound, injure);
        assertTrue(response.score > successScore);
        printResult(wound, injure, response);
    }
    
    @Test
    public void testWordnetPathSim() {
        WordNetPathSim wpsim = null;
        try {
        	wpsim = new WordNetPathSim();
        } catch (IOException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
        double successScore = 0.01;
        String dog = "dog";
        String animal = "animal";

        MetricResponse response = wpsim.compare(dog, animal);
        assertTrue(response.score > successScore);
        printResult(dog, animal, response);

        response = wpsim.compare(animal, dog);
        assertTrue(response.score < successScore);
        printResult(animal, dog, response);
    }
    
    @Test
    public void testWordSim() {
		WordSim wsimL = null;
		WordSim wsimR = null;
		try {
			wsimL = new WordSim(WnsimConstants.LIN);
			wsimR = new WordSim(WnsimConstants.RESNIK);
        } catch (IOException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
		
        double successScore = 0.01;
        String dog = "dog";
		String animal = "animal";

        MetricResponse response = wsimL.compareString(dog, animal);
        assertTrue(response.score > successScore);
        printResult(dog, animal, response);

        response = wsimL.compareString(animal, dog);
        assertTrue(response.score > successScore);
        printResult(animal, dog, response);
        
        response = wsimR.compareString(dog, animal);
        assertTrue(response.score > successScore);
        printResult(dog, animal, response);

        response = wsimR.compareString(animal, dog);
        assertTrue(response.score > successScore);
        printResult(animal, dog, response);
    }
    
    @Test
    public void testWnsimSimple() {
        WNSimSimple sim = null;
        try {
            sim = new WNSimSimple();
        } catch (IOException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
        double successScore = 0.01;
        String first = "apple";
        String second = "fruit";
        String third = "church";
        String fourth = "building";
        String fifth = "dog";
        String sixth = "animal";

        MetricResponse response = sim.compare(first, second);
        assertTrue(response.score > successScore);
        printResult(first, second, response);

        response = sim.compare(first, third);
        assertTrue(response.score < successScore);
        printResult(first, third, response);

        response = sim.compare(third, fourth);
        assertTrue(response.score > successScore);
        printResult(third, fourth, response);

        response = sim.compare(fifth, sixth);
        assertTrue(response.score > successScore);
        printResult(fifth, sixth, response);
    }
    
    private static void printResult(String first, String second, MetricResponse result) {
        System.out.println("Comparing '" + first + "' with '" + second + "': ");
        System.out.println("Result is: '" + result + "'.");
    }
}
