#!/bin/bash

MAINCLASS=edu.illinois.cs.cogcomp.wnsim.xmlrpc.WCClient

HOST=
PORT=

if [ $# -eq 2 ]; then
    HOST=$1
    PORT=$2
else
    echo "Usage: $0 HOST PORT"
    exit -1
fi


LIB=target/dependency
DIST=target

for JAR in `ls $DIST/*.jar`; do
    CP=$CP:$JAR
done

for JAR in `ls $LIB/*jar`; do
    CP=$CP:$JAR
done

CMD="java -Xmx1g -cp $CP $MAINCLASS $HOST $PORT"

echo "$0: running command '$CMD'..."

$CMD

