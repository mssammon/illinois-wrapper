#!/bin/bash

MAINCLASS=edu.illinois.cs.cogcomp.wnsim.xmlrpc.WCServer

HOST=
PORT=

if [ $# -eq 1 ]; then
    PORT=$1
else
    echo "Usage: $0 PORT"
    exit -1
fi

LIB=target/dependency
DIST=target

for JAR in `ls $DIST/*.jar`; do
    CP=$CP:$JAR
done

for JAR in `ls $LIB/*jar`; do
    CP=$CP:$JAR
done

CMD="java -Xmx4g -cp $CP $MAINCLASS $PORT"

echo "$0: running command '$CMD'..."

$CMD

